# _Matplotlib_ - Introduction

## Overview

* Teaching: 5 min
* Exercises: -
* Questions
  * What is _matplotlib_ and what do we use it for?
  * What elements does a plot have?
  * What is the nomenclature used by _matplotlib_?
* Objectives
  * Learn about the background of the _matplotlib_ framework
  * Understand which elements a plot is composed of
  * Understand the difference between axis and axes

---

## What is _matplotlib_?
* _Matplotlib_ ia a framework - i.e. a collection of functionality, not a program on its own
* The name comes from "Matrix Plotting Library" (not "Math…")

## What is _matplotlib_ used for?
Its main application case is generating data visualizations. 
The framework produces high quality images based on the data given and is very customizable.
It integrates well with _pandas_ or _numpy_.

## How to get _matplotlib_?
It can be installed via _pip_. 
Make sure that the dependencies are installed as well.

## Where to find help?
* Official Page: https://matplotlib.org/
* Documentation: https://matplotlib.org/stable/api/index
* Example gallery: https://matplotlib.org/stable/gallery/index.html

## Anatomy of a plot

In the following image you can find the naming of elements as used by _matplotlib_:
![image alt](https://matplotlib.org/stable/_images/anatomy.png)


## Nomenclature

* In the nomenclature of the framework there is a difference between
    * **Axis:** the number line that gets printed on the side of the plot
    * **Axes:** the collection of all plotted elements that represent data (roughly: the plotting area)

* An **artist** determines the rendering style of plot elements.
* The **figure** is the collection of all plot elements.
>    * Figures can be nested into other figures. The nested element is called a _sub-figure_.
>    * Implementation-wise, the _figure_ is an artist (i.e. it controls its own plotting style)
>    * The rectangle for the figure Background is contained in the attribute `patch`

---

## Key Points
* _matplotlib_ is a data visualization framework
* It has its very own nomenclature
* Pay attention to _axis_ versus _axes_

# Python Introduction - _For_ Loops

## Overview
* Teaching: 10 min
* Exercises: 15 min
* Questions
  * How can I make a program do repeated things?
* Objectives
  * Explain what for loops are normally used for.
  * Trace the execution of a simple (unnested) loop and correctly state the values of variables in each iteration.
  * Write for loops that use the Accumulator pattern to aggregate values.

## _Collection_ is a generalization of lists
* _Collection_ ~ "a bunch of things"
  * More specific representations are _lists_, _strings_(, _tuples_, _dicts_ …)

## A for loop executes commands once for each value in a collection.
* Doing calculations on the values in a list one by one is as painful as working with pressure_001, pressure_002, etc.
* A for loop tells Python to execute some statements once for each value in a list, a character string, or some other collection.
  * “for each thing in this group, do these operations”

```python
for number in [2, 3, 5]:
    print(number)
```

Also, notably you can pass in a list-variable:

```python
names = ["Anna", "Bert", "Claude"]
for current_name in names:
    print("Hello", current_name)
```

## A for loop is made up of a collection, a loop variable, and a body.
* The collection, `[2, 3, 5]`, is what the loop is being run on.
* The body, `print(number)`, specifies what to do for each value in the collection.
* The loop variable, `number`, is what changes for each iteration of the loop.
  * The “current thing”.

## The first line of the for loop must end with a colon, and the body must be indented.
* The colon at the end of the first line signals the start of a block of statements.
  * Python uses indentation to show nesting.
  * Any consistent indentation is legal, but almost everyone uses four spaces.

```python
for number in [2, 3, 5]:
print(number) # missing indentation

firstName = "Jon"
  lastName = "Smith" # illegal indentation
```

## Loop variables can be called anything.
* Just as regular variables

# The body of a loop can contain many statements.
* But no loop should be more than a few lines long.
* Hard for human beings to keep larger chunks of code in mind.

```python
primes = [2, 3, 5]
for p in primes:
    squared = p ** 2
    cubed = p ** 3
    print(p, squared, cubed)
```

## Use range to iterate over a sequence of numbers.
* The built-in function range produces a sequence of numbers.
  * Not a list: the numbers are produced on demand to make looping over large ranges more efficient.
  * `range(N)` is the numbers `0..N-1`
  * Exactly the legal indices of a list or character string of length `N`

```python
print('a range is not a list:', range(0, 3))
for number in range(0, 3):
    print(number)
```

## The Accumulator pattern turns many values into one.
* A common pattern in programs is to:
  * Initialize an accumulator variable to zero, the empty string, or the empty list.
  * Update the variable with values from a collection.

```python
# Sum the first 10 integers.
total = 0
for number in range(10):
   total = total + (number + 1)
print(total)
```

* Read total = total + (number + 1) as:
  * Add 1 to the current value of the loop variable number.
  * Add that to the current value of the accumulator variable total.
  * Assign that to total, replacing the current value.
* We have to add number + 1 because range produces 0..9, not 1..10.

# Key Points

* A for loop executes commands once for each value in a collection.
* A for loop is made up of a collection, a loop variable, and a body.
* The first line of the for loop must end with a colon, and the body must be indented.
* Indentation is always meaningful in Python.
* Loop variables can be called anything (but it is strongly advised to have a meaningful name to the looping variable).
* The body of a loop can contain many statements.
* Use range to iterate over a sequence of numbers.
* The Accumulator pattern turns many values into one.

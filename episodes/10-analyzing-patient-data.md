# NumPy - Analyzing Patient Data


## Overview
* Teaching: 30 min
* Exercises: 20 min
* Questions
  * How can I process tabular data files in Python?
* Objectives
  * Read tabular data from a file into a program.
  * Select individual values and subsections from data.
  * Perform operations on arrays of data.

## Loading Data Into Python

```python
import numpy

data = numpy.loadtxt(fname='inflammation-01.csv', delimiter=',')
print(data)
```
```
array([[ 0.,  0.,  1., ...,  3.,  0.,  0.],
       [ 0.,  1.,  2., ...,  1.,  0.,  1.],
       [ 0.,  1.,  1., ...,  2.,  1.,  1.],
       ...,
       [ 0.,  1.,  1., ...,  1.,  1.,  1.],
       [ 0.,  0.,  0., ...,  0.,  2.,  0.],
       [ 0.,  0.,  1., ...,  1.,  1.,  0.]])
```

> Let a participant explain what is happening here

```python
print(type(data))
```
```
<class 'numpy.ndarray'>
```

* Data currently refers to an N-dimensional array, 
  * Provided by the NumPy library
   * These data correspond to arthritis patients’ inflammation. 
   * Rows are the individual patients
   * Columns are their daily inflammation measurements.

## Underlying Data Type

* A Numpy array contains one or more elements of the same type. 
* The `type` function will only tell you that a variable is a NumPy array but won’t tell you the type of thing inside the array. 
* We can find out the type of the data contained in the NumPy array.

```python
print(data.dtype)
```
```
float64
```

## Data shape
```python
print(data.shape)
```
```
(60, 40)
```
![Array Layout Visualization from Carpentries](https://swcarpentry.github.io/python-novice-inflammation/fig/python-zero-index.png)
* 60 rows and 40 columns. 
* `data.shape` is an attribute
  * Attribute: Value that belongs to a variable
  * Compare Method <-> Function

## Accessing Singular Data

```python
print('first value in data:', data[0, 0])
print('middle value in data:', data[30, 20])
```

* `data[30, 20]` accesses the element at row 30, column 20.
* **Compare to what we learned about lists**

## Slicing data

```python
print(data[0:4, 0:10])
print(data[5:10, 0:10])
```

* If not given explicitly, the first and the last element will be used as default for a slice 

```python
small = data[:3, 36:]
print('small is:')
print(small)
```
The above example selects rows 0 through 2 and columns 36 through to the end of the array.

## Analyzing data

* Compute mean value of the data

```python
print(numpy.mean(data))
```
```python
maximum = numpy.max(data)
minimum = numpy.min(data)
standard_derivation = numpy.std(data)

print('maximum inflammation:', maximum)
print('minimum inflammation:', minimum)
print('standard deviation:', standard_derivation)
```
* Get the data for a specific patient
```python
patient_0 = data[0, :] # 0 on the first axis (rows), everything on the second (columns)
print('maximum inflammation for patient 0:', numpy.max(patient_0))
```
Or, as inline function call:
```python
print('maximum inflammation for patient 2:', numpy.max(data[2, :]))
```

## Getting Data for all Rows / Columns

* Already have the means to do it "by hand"
* _Numpy_ has a feture for this: The `axis`-parameter
  * Per day (column-wise) -> `axis = 0`
  * Per-patient (row-wise)  -> `axis = 1`

  ![Axis visualization from Carpentries](https://swcarpentry.github.io/python-novice-inflammation/fig/python-operations-across-axes.png)

```python
mean_per_day = numpy.mean(data, axis=0)
print(mean_per_day)
# For demonstration, look at the shape
print(mean_per_day.shape)
```
```
[  0.           0.45         1.11666667   1.75         2.43333333   3.15
   3.8          3.88333333   5.23333333   5.51666667   5.95         5.9
   8.35         7.73333333   8.36666667   9.5          9.58333333
  10.63333333  11.56666667  12.35        13.25        11.96666667
  11.03333333  10.16666667  10.           8.66666667   9.15         7.25
   7.33333333   6.58333333   6.06666667   5.95         5.11666667   3.6
   3.3          3.56666667   2.48333333   1.5          1.13333333
   0.56666667]

(40,)
```
* Have an N×1 vector
  * Average inflammation per day for all patients

```python
print(numpy.mean(data, axis=1))
```
* Gives average inflammation per patient across all days.

# Key Points

* Use the numpy library to work with arrays in Python.
* The expression array.shape gives the shape of an array.
* Use `array[x, y]` to select a single element from a 2D array.
* Use `numpy.mean(array)`, `numpy.max(array)`, and `numpy.min(array)` to calculate simple statistics.
* Use `numpy.mean(array, axis=0)` or `numpy.mean(array, axis=1)` to calculate statistics across the specified axis.

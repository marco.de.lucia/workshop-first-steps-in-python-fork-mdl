# _Pandas_ - Dataframes and Series

## Overview

* Teaching: 15 min
* Exercises: -
* Questions
  * What are _Dataframes_ in _pandas_?
  * What are _Series_ in _pandas?
* Objectives
  * Get acquainted with the central data structures of the framework

---

## Introducing _Series_

* _Series_ in _pandas_ represent 1-dimensional data, i.e. a sequence of values
* It is provided as it's own data type by _pandas_
* _Series_ are often used to represent values changing over time
* The values within a _series_ do usually have the same data type
* Each of the values can have an index associated with it

To get import to the _Series_ run:

```python
from pandas import Series  # Note the initial upper-case letter
```

### Creating _Series_ from various kinds of Data

Let's say we have a cat and we noticed it is sneezing a lot.
We suspect it might be allergic to something.
So we track the count of sneezes over one week.

```python
sneeze_counts = Series(data=[32, 41, 56, 62, 30, 22, 17])
print(sneeze_counts)
```
Output:
```
0    32
1    41
2    56
3    62
4    30
5    22
6    17
dtype: int64
```
* The _Series_ automatically adds an index on the left side
* It also automatically infers the best fitting data type for the elements (here `int64` = 64-bit integer)

To make the data a bit more meaningful, let's set a custom index:
```python
days_of_week = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]
sneeze_counts.index = days_of_week
print(sneeze_count)
```
Output:
```
Monday       32
Tuesday      41
Wednesday    56
Thursday     62
Friday       30
Saturday     22
Sunday       17
dtype: int64
```
Also, we add a name to the series, so we can distinguish it later:
```python
sneeze_counts.name = "Sneezes"
```

* The index and name can also be passed directly while creating the series

We suspect that the illness of our cat is related to the weather, so we also log the average temperature and humidity 
```python 
temperatures = Series(data=[10.9, 8.2, 7.6, 7.8, 9.4, 11.1, 12.4], index=days_of_week, name="Temperature")
humidities = Series(data=[62.5, 76.3, 82.4, 98.2, 77.4, 58.9, 41.2], index= days_of_week, name="Humidity")
```

> **Note:** Alternatively you can provide the index while creating the _series_ by passing a dictionary:
> ```python
> sneeze_counts = Series(
>     data= {
>         "Monday": 32,
>         "Tuesday": 41,
>         "Wednesday": 56,
>         "Thursday": 62,
>         "Friday": 30,
>         "Saturday": 22,
>         "Sunday": 17
>     },
>     name="Sneezes"
> )
> ```

* To get a first statistical impression of the data, use the `describe()`-method:
```python
print(temperatures.describe())
```
Output:
```
count     7.000000
mean      9.628571
std       1.871465
min       7.600000
25%       8.000000
50%       9.400000
75%      11.000000
max      12.400000
Name: Temperature, dtype: float64
```

## Introducing _Dataframes_

To correlate our various measurements, we want some table-like data structure, so we import _Dataframes_:
```python
from pandas import DataFrame  # Note the camel-case spelling
```

* A _dataframe_ can be created from a list of _series_, where each _series_ forms a **row** in the resulting table 
```python
measurements = DataFrame(data=[sneeze_counts, temperatures, humidities])
print(measurements)
```
Output:
```
             Monday  Tuesday  Wednesday  Thursday  Friday  Saturday  Sunday
Sneezes        32.0     41.0       56.0      62.0    30.0      22.0    17.0
Temperature    10.9      8.2        7.6       7.8     9.4      11.1    12.4
Humidity       62.5     76.3       82.4      98.2    77.4      58.9    41.2
```

* A _dataframe_ can be created from a dictionary of _series_ where each _series_ forms a **column** in the resulting table 
```python
measurements = DataFrame(
  data={
    sneeze_counts.name: sneeze_counts,
    temperatures.name: temperatures,
    humidities.name: humidities
  }
)
print(measurements)
```
Output:
```
           Sneezes  Temperature  Humidity
Monday          32         10.9      62.5
Tuesday         41          8.2      76.3
Wednesday       56          7.6      82.4
Thursday        62          7.8      98.2
Friday          30          9.4      77.4
Saturday        22         11.1      58.9
Sunday          17         12.4      41.2
```

* To flip rows and columns, _dataframes_ can be transposed using the `T`-property:
```python
column_wise = DataFrame(data=temperatures)
print(column_wise)

print()  # Add a blank line as separator

row_wise = column_wise.T
print(row_wise)
```
Output:
```
           Temperature
Monday            10.9
Tuesday            8.2
Wednesday          7.6
Thursday           7.8
Friday             9.4
Saturday          11.1
Sunday            12.4

             Monday  Tuesday  Wednesday  Thursday  Friday  Saturday  Sunday
Temperature    10.9      8.2        7.6       7.8     9.4      11.1    12.4
```
* Note: Store the transposed dataframe in a new variable, the original will not be changed.


---

## Key Points
* _Series_ represent 1-dimensional data
* _Dataframes_ represent 2-dimensional (tabular) data
* Each column in a _dataframe_ is a _series_
* _dataframes_ and _series_ have row indices to label the data
* _Dataframes_ may be transposed to switch rows and columns

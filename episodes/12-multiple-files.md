# NumPy - Multiple Files

* Overview
* Teaching: 20 min
* Exercises: 0 min
* Questions
  * How can I do the same operations on many different files?
* Objectives
  * Use a library function to get a list of filenames that match a wildcard pattern.
  * Write a for loop to process multiple files.

```python
import glob
print(glob.glob('inflammation*.csv'))
```
* `glob`, finds files and directories whose names match a pattern.
> See also the shell lesson!

## Intermezzo: Writing a Program in a File

* Writing a program into a file allows it to be re-run without having to type it in all over again
* The file should end with `.py`
* Make sure to use a plaintext editor

```bash
nano plot_data.py
```
```python
import glob
import numpy
import matplotlib.pyplot

filenames = sorted(glob.glob('inflammation*.csv'))

# Limit the analysis to the first three files
filenames = filenames[0:3]

for filename in filenames:
    print(filename)

    data = numpy.loadtxt(fname=filename, delimiter=',')

    figure = matplotlib.pyplot.figure(figsize=(10.0, 3.0))

    subplot0 = figure.add_subplot(1, 3, 1)
    subplot1 = figure.add_subplot(1, 3, 2)
    subplot2 = figure.add_subplot(1, 3, 3)

    subplot0.set_ylabel('average')
    subplot0.plot(numpy.mean(data, axis=0))

    subplot1.set_ylabel('max')
    subplot1.plot(numpy.max(data, axis=0))

    subplot2.set_ylabel('min')
    subplot2.plot(numpy.min(data, axis=0))

    figure.tight_layout()
    matplotlib.pyplot.show()

```
* **Make sure the file ends with an empty line!**
The file can then be run with:

```bash
python plot_data.py
```

# Key Points
* Use glob.glob(pattern) to create a list of files whose names match a pattern.
* Use * in a pattern to match zero or more characters, and ? to match any single character.
* Code can be put into files and run with `python`



# Getting started with _pyplot_

## Overview

* Teaching: 15 min
* Exercises: -
* Questions
    * What is _pyplot_?
    * How to create simple plots
* Objectives
    * Learn to create inital plots with _pyplot_
    * Understand the _imperative_ approach

---

## Creating simple plots

The _matplotlib_ framework offers _pyplot_ as a set pf convenient shortcuts.

```python
from matplotlib import pyplot
from pandas import Series

# Also have some data for later
sneeze_counts = Series(
    data= {
        "Monday": 32,
        "Tuesday": 41,
        "Wednesday": 56,
        "Thursday": 62,
        "Friday": 30,
        "Saturday": 22,
        "Sunday": 17
    },
    name="Sneezes"
)
```

To get a simple plot, one can feed the data into the plotting function:

```python
pyplot.plot(sneeze_counts)  # Generate a plot from the data
pyplot.show()      # Make pyplot actually display the plot
```

**Note:** _pyplot_ internally keeps track of state of the plot. You as the user only get to see it once you call the `show()` function.
This way of programming is calles _imperative_.

**Note:** There is also a different approach to things, which is called _object oriented_ which is used in the underlying workings of _matplotlib_. For clarity reasons we will show _pyplot_ first and then expand upon that.

> To help with understanding, imagine you want to have a cake.
> _Imperative_ style is like calling a baker and describing how you want your cake to look like.
> _Object oriented_ style is like ordering the ingredients and then modifying the soon-to-be-cake until it looks like you want it.

**Note:** After calling `pyplot.show()`, _pyplot_ drops everything about the plo you just set up. If you find yourself recreating plots often, consider moving the plot creation into a function and putting it into a file to make your life a bit easier.

## Adding labels

While plotting we want to have some useful labelling, so we can expand on the example:

* First we are going to use a bar chart since our data is not continuous
    * The `bar()`-function wants an explicit set of labels to use, so we pass in the index of our series as well as the series itself.
* Then we set a title for the axis and the plot itself

```python
pyplot.bar(sneeze_counts.index, sneeze_counts)
pyplot.xlabel("Day of the Week")
pyplot.ylabel(sneeze_counts.name)
pyplot.title("Analyzing our cats health")
pyplot.show()
```

## Plot Types

_matplotlib_ supports a wide variety of plotting functions. Here is a selection that is easily accessible through _pyplot_.

| Plot type    | `pyplot.` Function |
|--------------|--------------------|
| Line plout   | `plot()`           |
| Scatter plot | `scatter()`        |
| Bar plot     | `bar()`            |
| Box plot     | `boxplot()`        |
| Stem plot    | `stem()`           | 
| Histogram    | `hist()`           |
| Polar plot   | `polar()`          |
| Matrix plot  | `imshow()`         |

* See also the [_pyplot_ documentation](https://matplotlib.org/stable/api/pyplot_summary.html)

## Working with DataFrames

_Matplotlib_ can also handle _pandas_ `DataFrame`s as input:

```python
from pandas import DataFrame
from matplotlib import pyplot

measurements = DataFrame(
    data = {
        "Outside Temperature": [10.9, 8.2, 7.6, 7.8, 9.4, 11.1, 12.4],
        "Inside Temperature": [19.3, 20.1, 18.9, 20.6, 19.0, 21.2, 21.5]
    },
    index = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]
)

pyplot.plot(measurements)
pyplot.legend(measurements.columns.values)
pyplot.show()
```

## Dealing with Multiple Plots

If we want to have multiple plots in our figure, we can do so as well:

```python
# Create some values to plot
values = Series(range(10))

# We want to arrange our plot in 1 row and 2 columns (i.e. have 2 plots)
figure, axes = pyplot.subplots(nrows=1, ncols=2)
pyplot.suptitle("Comparing different functions")

# Select and create the first plot
pyplot.sca(axes[0])
pyplot.title("Quadratic function")
pyplot.plot(values, values ** 2)

# Select and create the second plot
pyplot.sca(axes[1])
pyplot.title("Linear function")
pyplot.plot(values, values * 10)

pyplot.show()
```

---

## Key Points

* _pyplot_ is a module in the _matplotlib_ framework
* It offers a variety of utility functions to quickly generate some basic plots

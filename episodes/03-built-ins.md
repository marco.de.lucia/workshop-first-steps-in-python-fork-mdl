# Python Introduction - Built-In Functions

## Overview
* Teaching: 15 min
* Exercises: 10 min
* Questions
  * How can I use built-in functions?
  * How can I find out what they do?
  * What kind of errors can occur in programs?
* Objectives
  * Explain the purpose of functions.
  * Correctly call built-in Python functions.
  * Correctly nest calls to built-in functions.
  * Use help to display documentation for built-in functions.
  * Correctly describe situations in which SyntaxError and NameError occur.

## A function may take zero or more arguments.

* An argument is a value passed into a function.
  * `len` takes exactly one.
  * `int`, `str`, and `float` create a new value from an existing one.
  * `print` takes zero or more.
  * `print` with no arguments prints a blank line.
    * **Must always use parentheses**, even if they’re empty, so that Python knows a function is being called.

```python
print('before')
print()
print('after')
```
```
before

after
```

## Commonly-used built-in functions include max, min, and round.

* Use `max` to find the largest value of one or more values.
* Use `min` to find the smallest.
  * Both work on character strings as well as numbers.
    * “Larger” and “smaller” use (0-9, A-Z, a-z) to compare letters.

```python
print(max(1, 2, 3))
print(min('a', 'A', '0'))
```
```
3
0
```

## Functions may only work for certain (combinations of) arguments.

* `max` and `min` must be given at least one argument.
  * “Largest of the empty set” is a meaningless question.
  * And they must be given things that can meaningfully be compared.

```python
print(max(1, 'a'))
```
```
TypeError                                 Traceback (most recent call last)
<ipython-input-52-3f049acf3762> in <module>
----> 1 print(max(1, 'a'))

TypeError: '>' not supported between instances of 'str' and 'int'
```

## Functions may have default values for some arguments.

* `round` will round off a floating-point number.
  * By default, rounds to zero decimal places.
  * We can specify the number of decimal places we want.

```python
a_number = 3.712
round(a_number)
round(a_number, 1)
```
```
4
3.7
```

## Use the built-in function help to get help for a function.
* Every built-in function has online documentation.
```python
help(round)
```

## Python reports a syntax error when it can’t understand the source of a program.
* Won’t even try to run the program if it can’t be parsed.
* The message indicates where the problem happens

```python
# Forgot to close the quote marks around the string.
name = 'Feng

# An extra '=' in the assignment.
age = = 52

# Forgot to close parenthesis
print("hello world"
```

## Python reports a runtime error when something goes wrong while a program is executing.

```python
age = 53
remaining = 100 - aege # mis-spelled 'age'
```

## Key Points
* A function may take zero or more arguments.
* Commonly-used built-in functions include max, min, and round.
* Functions may only work for certain (combinations of) arguments.
* Functions may have default values for some arguments.
* Use the built-in function `help` to get help for a function.
* Every function returns something.
* Python reports a syntax error when it can’t understand the source of a program.
* Python reports a runtime error when something goes wrong while a program is executing.
* Fix syntax errors by reading the source code, and runtime errors by tracing the program’s execution.


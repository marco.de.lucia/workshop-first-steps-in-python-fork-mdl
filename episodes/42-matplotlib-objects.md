# Working with Objects

As we have seen before, the `subplots()`- function actually returns two values.
These are variables from a custom data type (aka. class) defined by _matplotlib_.
We call those variable _objects_ hence the name.

```python
# Create some sample data
x = Series(range(0, 100)) / 50

# Note that even in the OO-style, we use `.pyplot.figure` to create the figure.
figure, axes = pyplot.subplots()

# Note that the function names for the axes differ from those of pyplot sometimes
axes.set_title("Comparing functions")
axes.plot(x, x, label="linear")
axes.plot(x, x**2, label="quadratic")
axes.plot(x, x**3, label="cubic")
axes.set_xlabel("x")
axes.set_ylabel("y")
axes.legend()

pyplot.show()
```

# _Pandas_ - Introduction

## Overview

* Teaching: 5 min
* Exercises: -
* Questions
  * What is _pandas_ and what do we use it for?
* Objectives
  * Learn about the background of the _pandas_ framework

---

## What is _pandas_?
* _Pandas_ ia a framework - i.e. a collection of functionality, not a program on its own
* _Pandas_ is based on the numerical mathmatics framework _numpy_
    * Compared to _numpy_ it offers more usability and convenience but sacrifices speed

## What is _pandas_ used for?
Its main application cases is **data processing**.
This includes:
* Reading, exploring, cleaning, transforming and visualizing data

Common areas that make use of it are:
* Data Science
* Machine Learning

## How to get _pandas_?
It can be installed via _pip_. 
Make sure that the dependencies are installed as well.

## Where to find help?
* Official Documentation: https://pandas.pydata.org/docs/

---

## Key Points
* _pandas_ is based on _numpy_
* It offers additional utility functions but sacrifices speed

## Episode 4  - Lists
### Individual Exercises

#### Exercise 4.1 - Fill in the Blanks

Fill in the blanks so that the program below produces the output shown.
```python
values = ____
values.____(1)
values.____(3)
values.____(5)
print('first time:', values)
values = values[____]
print('second time:', values)
```
**Output**
```
first time: [1, 3, 5]
second time: [3, 5]
```
##### Post your solutions:

*
*

#### Exercise 4.2 - How Large is a Slice?

If ‘low’ and ‘high’ are both non-negative integers, how long is the list values[low:high]?

##### Post your solutions:

*
*

### Breakout Rooms

**Hint:** If you can not see what a program does by reading the code alone, you may just try and run it. :sunglasses:

#### Exercise 4.3: From Strings to Lists and Back

Given this:
```python
print('string to list:', list('tin'))
print('list to string:', ''.join(['g', 'o', 'l', 'd']))
```

1) What does `list('some string')` do?
2) What does `'-'.join(['x', 'y', 'z'])` generate?

##### Answers from Breakout Rooms:
###### Room 1
1)
2)

###### Room 2
1)
2)

###### Room 3
1)
2)

#### Exercise 4.4: Working With the End

What does the following program print?
```python
element = 'helium'
print(element[-1])
```
From this, try to find an answer to the following questions:

1) How does Python interpret a negative index?
2) If a list or string has `N` elements, what is the most negative index that can safely be used with it, and what location does that index represent?
3) If `values` is a list, what does `del values[-1]` do?
4) How can you display all elements but the last one without changing `values`? (Hint: you will need to combine slicing and negative indexing.)

##### Answers from Breakout Rooms:
###### Room 1
1)
2)
3)
4)

###### Room 2
1)
2)
3)
4)

###### Room 3
1)
2)
3)
4)

#### Exercise 4.5 - Stepping Through a List

What does the following program print?
```python
element = 'fluorine'
print(element[::2])
print(element[::-1])
```
1) If we write a slice as `low:high:stride`, what does `stride` do?
2) What expression would select all of the even-numbered items from a collection?

##### Answers from Breakout Rooms:
###### Room 1
1)
2)

###### Room 2
1)
2)

###### Room 3
1)
2)

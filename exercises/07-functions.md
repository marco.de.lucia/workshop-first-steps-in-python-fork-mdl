## Episode 7 - Functions
### Individual Exercises

#### Exercise 7.1 - Spot the Mistake

What is the mistake in the following piece of code?

```python
my_number = choose_number()
print(my_random_number)

def choose_number():
    return 4  # randomly chosen by dice roll
```

##### Post your solutions:

#### Exercise 7.2 - Write Your own Function

The following function should calculate the circumference of a rectangle given the sides as `width` and `height`.
Fill in the blanks.
```python
def calculate_circumference(____):
    ____ = ____
    return ____

print(calculate_circumference(width = 10, height = 12))
```
**Expected Output:**
```
44
```

##### Post your solutions:

#### Exercise 7.3 - Beatbox

Write a function called `beatbox(…)` that takes a number and prints output as follows:
* If this number can be divided by _3_ without remainder, print the text `"Dum"`.
* If the number can be divided by _5_ without remainder, print the text `"Tsss"`
* If the number can be divided by _3_ and by _5_  without remainder print the text `"DumTsss"`
* Otherwise print nothing

You can test your function with the following code:
```python
for number in range(0, 20):
    beatbox(number)
```
**Expected Output:**
```
DumTsss
Dum
Tsss
Dum
Dum
Tsss
Dum
DumTsss
Dum
```

> **Hint:** You can check if a number can be divided by another number by using the `%`-operator (called _modulo_), which gives you the remainder of a division.
> If `a % b == 0` then `a` could be divided by `b` without remainder.

##### Post your solutions


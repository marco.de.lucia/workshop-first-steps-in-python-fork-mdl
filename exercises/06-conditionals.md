## Episode 6  - Conditionals
### Individual Exercises
#### Exercise 6.1: Tracing Execution

What does this program print?
```python
pressure = 71.9
if pressure > 50.0:
    pressure = 25.0
elif pressure <= 50.0:
    pressure = 0.0
print(pressure)
```

##### Post your solutions:

*
*

#### Exercise 6.2 - Trimming Values

Fill in the blanks so that this program creates a new list containing `0` where the original list’s values were negative and `1` where the original list’s values were positive.

```python
original = [-1.5, 0.2, 0.4, 0.0, -1.3, 0.4]
result = ____
for value in original:
    if ____:
        result.append(0)
    else:
        ____
print(result)
```
**Desired Output:**
```
[0, 1, 1, 1, 0, 1]
```
##### Post your solutions:

*
*

#### Exercise 6.3 - Initializing

Modify this program so that it finds the largest and smallest values in the list no matter what the range of values originally is.

```python
values = [...some test data...]
smallest = None
largest = None
for v in values:
    if ____:
        smallest = v
        largest = v
    ____:
        smallest = min(____, v)
        largest = max(____, v)
print(smallest, largest)
```

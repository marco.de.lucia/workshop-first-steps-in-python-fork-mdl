## Exercise 10 - Analyzing Patient Data

### Breakout Rooms

#### Exercise 10.1 - Change In Inflammation

The patient data is longitudinal in the sense that each row represents a series of observations relating to one individual. 
This means that the change in inflammation over time is a meaningful concept. 
Let us find out how to calculate changes in the data contained in an array with NumPy.

The `numpy.diff()` function takes an array and returns the differences between two successive values. 
Let us use it to examine the changes each day across the first week of patient 3 from our inflammation dataset.

```python
patient3_week1 = data[3, :7]
print(patient3_week1)
```
gives
```
 [0. 0. 2. 0. 4. 2. 2.]
```

Calling `numpy.diff(patient3_week1)` would do the following calculations
```
[ 0 - 0, 2 - 0, 0 - 2, 4 - 0, 2 - 4, 2 - 2 ]
```
and return the 6 difference values in a new array.

```python
print(numpy.diff(patient3_week1))
```
**Output:**
```
[ 0.,  2., -2.,  4., -2.,  0.]
```

Note that the array of differences is shorter by one element (length 6).

When calling `numpy.diff` with a multi-dimensional array, an axis argument may be passed to the function to specify which axis to process.

##### Consider the following questions:
1. When applying `numpy.diff` to our 2D inflammation array data, which axis would we specify?
2. If the shape of an individual data file is `(60, 40)` (60 rows and 40 columns), what would the shape of the array be after you run the `diff()` function and why?
3. How would you find the largest change in inflammation for each patient? Answer with the relevant piece of code.
  * Does it matter if the change in inflammation is an increase or a decrease? How does the code need to be adapted in either case?
  > **Hint:** There is a function called `numpy.absolute(…)`

##### Answers from Breakout Rooms:
###### Room 1
1)
2)
3)

###### Room 2
1)
2)
3)

###### Room 3
1)
2)
3)

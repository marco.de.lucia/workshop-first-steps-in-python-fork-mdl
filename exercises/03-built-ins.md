## Episode 3  - Built-In Functions
### Individual Exercises

#### Exercise 3.1: What Happens When?

1) Explain in simple terms the order of operations in the following program: when does the addition happen, when does the subtraction happen, when is each function called, etc.
```python
radiance = 1.0
radiance = max(2.1, 2.0 + min(radiance, 1.1 * radiance - 0.5))
```
2) What is the final value of radiance?

##### Post your solutions:

*
*

#### Exercise 3.2 - Spot the Difference

1) Predict what each of the print statements in the program below will print.
```python
easy_string = "abc"
print(max(easy_string))
rich = "gold"
poor = "tin"
print(max(rich, poor))
print(max(len(rich), len(poor)))
```
2) Does `max(len(rich), poor)` run or produce an error message? If it runs, does its result make any sense?

##### Post your solutions:

*
*

#### Exercise 3.3 - Last Character of a String
If Python starts counting from zero, and `len(…)` returns the number of characters in a string, what index expression will get the last character in the string name?

##### Post your solutions:

*
*

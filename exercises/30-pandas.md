# Episode 30 - Pandas

This is a stand-alone open-end exercise to practise working with _pandas_ on a real-live data set.

> It is highly recommended to have the _pandas_ documentation open for this exercise, it will be needed a lot.
> Not all required functions will be listed in the exercise document, finding out what to use is part of the intended training.

## Getting the Data

The [NOAA][noaa] provides open weather data for stations around the world.
* Here is a [list of all stations][stations] and their respective codes
    * This list in itself already makes for an interesting data set to explore
* This is the [weather data archive][archive].
    * It is sorted by year and then station code from the station list

### Tasks
* Pick, download and extract a sample data set from the archive.
    * If you are not sure, _New York, Central Park_ (Code 725060) in 2020 would be a good pick
    * Visually inspect the data set with a text editor to make sure it does not only contain a few rows of data
    * Get acquainted with the [ISD Lite data format][documentation], it holds valuable information how to interpret what you have in front of you


## Loading the Data
* For loading, the `pandas.read_csv()`-function can be used. 
    * [`read_csv()` documentation][pandas-read-csv-doc]
* Note that for these data sets the seperator is not a comma, but multiple whitespaces
    * The [regular expression][regex] `"\s+"` can be used to describe the separator
* Note the parameter `parse_dates` which can come in extremely handy
* Note that the data set as provided has **no header**

### Tasks
* Consider **first** what the loaded dats should look like 
* Load the data set
    * Display the loaded data, compare with your expectations and do a plausability check
* Assign a proper header based on the information from the [data documentation][documentation]

## Cleaning the data
* According to the data documentation, the value `-9999` indicates missing data
* Some data columns have been scaled by a factor

### Tasks
* Replace the value `-9999` with something more appropriate
    * A suggestion would be the constant `nan` from the `math` library
* Check for columns that have no data at all and remove them if convenient
* Re-scale the columns so they all use a factor of 1 (and can be read and interpreted more easily by humans)
* Check if there are entries missing for some dates/hours.
    * Consider how many hours the given year should have
    * Add placeholders for those missing rows, so the averading works as expected.

## Initial Exploration

### Tasks
* Find the most extreme temperatures, wind speeds amd precipitations measured
* Calculate the year average for temperatures, pressures and wind speed
* Find the total precipitation for the year

## Advanced Tasks

### Tasks
* Calculate the differences in air temperature from one hour to the next
    * Find the biggest rise/ drop in temperature over an hour and when they happened
* Find out what the most common wind directions in your location are
    * Note: Wind directions can be rather fuzzy, so you might want to come with a better metric here than simply calculating the maximum
    * Note: A direction of `0` means that it is _undetermined_, _North_ is designated by `360`
    * Note: The Wind direction wraps around after `360` to `10` (direction is given in incremets of 10°) take this into account. 
        * You might find the _modulo_-operator useful (`%` in python)
* Extrapolate daily statistics from the hourly ones
* Come up with a metric for a "nice day" and an "awful day". What were the most _nice_ or _awful_ days in your location? 


[noaa]: https://en.wikipedia.org/wiki/National_Oceanic_and_Atmospheric_Administration
[stations]: https://www.ncei.noaa.gov/pub/data/noaa/isd-history.txt
[archive]: https://www1.ncdc.noaa.gov/pub/data/noaa/isd-lite/
[documentation]: https://www1.ncdc.noaa.gov/pub/data/noaa/isd-lite/isd-lite-format.pdf
[pandas-read-csv-doc]: https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.read_csv.html
[regex]: https://en.wikipedia.org/wiki/Regular_expression

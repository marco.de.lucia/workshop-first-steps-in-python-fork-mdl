# Episode 12 - Multiple Files

### Individual Exercise

#### Plotting Differences

Plot the difference between the _average_ inflammations reported in the first and second datasets (stored in `inflammation-01.csv` and `inflammation-02.csv`, correspondingly), i.e., the difference between the leftmost plots of the first two figures.

> Remember, with _numpy_ you can just subtract data sets from each other.

#### Generate Composite Statistics

Use each of the files once to generate a dataset containing values averaged over all patients:

```python
filenames = glob.glob('inflammation*.csv')
composite_data = numpy.zeros((60,40)) # Generates an array with all 0
for filename in filenames:
    # sum each new file's data into composite_data as it's read
    #
    # and then divide the composite_data by number of samples
composite_data = composite_data / len(filenames)
```

Then use pyplot to generate average, maximum, and minimum for all patients.

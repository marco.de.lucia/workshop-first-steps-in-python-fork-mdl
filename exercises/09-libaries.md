## Episode 9 - Libraries
### Individual Exercises

#### Exercise 9.1 - When Is Help Available?

When a colleague of yours types help(math), Python reports an error:
```
NameError: name 'math' is not defined
```
What has your colleague forgotten to do?

##### Post your solutions:

*
*

#### Exercise 9.2 - Locating the Right Module

You want to select a random character from a string:
```python
bases = 'ACTTGCTTGAC'
```
1) Which standard library module could help you? How can you find out?
2) Which function would you select from that module? Are there alternatives?
3) Try to write a program that uses the function to select a random character from a string.

##### Post your solutions:

*
*

## Episode 8 - Variable Scopes
### Breakout Rooms

#### Exercise 8.1 - Scopes
Consider the following program:

```python
density = 1.0                          # Line 1
weight = 1500                          # Line 2

def calculate_density(weight, volume): # Line 4
    density = weight / volume          # Line 5
    return density                     # Line 6

print(calculate_density(weight, 1000)) # Line 8
print(density)                         # Line 9
```
1) Trace the program execution. Decide whether variables are in a local or global scope and what value they have.
2) What will the program print?

:arrow_down: You can copy this table for your answer :arrow_down:
| Line | `density` =  | `weight` =    |
|------|--------------|---------------|
|   1  | 1.0 (global) |      —        |
|      |              |               |
|      |              |               |
|      |              |               |
|      |              |               |
|      |              |               |
|      |              |               |
|      |              |               |

##### Answers from Breakout Rooms:
###### Room 1
1)
2)

###### Room 2
1)
2)

###### Room 3
1)
2)
